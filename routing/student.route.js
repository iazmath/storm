'use strict';

let studentController  = require('../controllers/student.controller');

module.exports = (app) =>{
    app.get('/api/students', studentController.search);
    app.post('/api/student', studentController.create);
    app.put('/api/student/update/:id', studentController.update);
    app.delete('/api/student/delete/:id',studentController.delete);
}