'use strict';

module.exports = (app) =>{

    require('./user.route')(app);
    require('./student.route')(app);

};

