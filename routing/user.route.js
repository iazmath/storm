'use strict';

let userController  = require('../controllers/user.controller');

module.exports = (app) =>{
    app.route("/api/users").get(userController.search);
    //app.get('/api/users/:id',userController.search);
    app.route("/api//user").post(userController.create);
    app.route("/api/users/update").put(userController.update);
    app.route("/api/users/delete").put(userController.delete);
}