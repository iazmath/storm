'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('t_users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      first_name: {
        allowNull: false,
        type: Sequelize.STRING
      },
      last_name: {
        allowNull: false,
        type: Sequelize.STRING
      },
      email: {
        allowNull: false,
        type: Sequelize.STRING
      },
      username:{
        allowNull: false,
        type: Sequelize.STRING
      },
      password:{
        allowNull: true,
        type: Sequelize.STRING
      },
      user_status : {
        allowNull: true,
        type: Sequelize.ENUM,
        values: ['pending', 'active', 'suspended', 'deleted'],
        defaultValue : 'pending',
      },
      user_verification_status : {
        allowNull: false,
        type: Sequelize.ENUM,
        values: ['pending', 'verified', 'un_verified'],
        defaultValue : 'pending',
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('t_users');
  }
};