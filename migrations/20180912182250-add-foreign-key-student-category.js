'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('t_students', 'student_category_id',{
        type : Sequelize.INTEGER,
        allowNull : false
      }
      ),
      queryInterface.addConstraint('t_students', ['student_category_id'], {
        type: 'foreign key',
        name: 'fk_student_reference_student_category',
        references: { //Required field
          table: 't_student_categories',
          field: 'id',
          
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      })
    ]);
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
