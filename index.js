const express = require("express");
// Import Body parser
const bodyParser = require('body-parser');

const app = express();

const port = process.env.PORT || 3000 ;

// Configure bodyparser to handle post requests
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

//importing route
const routes = require('./routing/index.route');
routes(app);

app.listen(port);

console.info('Storm RESTful API server started on: ' + port);
