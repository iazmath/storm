'use strict';

const student = require('../models/index').student;
const studentCategory = require('../models/index').studentCategory;

module.exports = {
    findAll : function(){
        return student.findAll({include: [{model: studentCategory}]});
    },
    save : function(stu_obj){
        return student.create(stu_obj);
    }
};