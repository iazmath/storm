'use strict';
//services
const studentService = require('../services/student.service')

module.exports = {
    
    search : function(request, response, next){
        studentService.findAll().then(result => response.json(result));
    },
    create : function(request, response, next) {
        console.log(request.body);
        studentService.save(request.body).then(student => response.json(student))
    },
    update : function(request, response, next){

    },
    delete : function(request, response, next) {

    }
};