'use strict';
module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    id: {
      field: 'id',
      type : DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    firstName: {
      field: 'first_name',
      type: DataTypes.STRING,
      allowNull: false,
    },
    lastName: {
      field: 'last_name',
      type: DataTypes.STRING,
      allowNull: false,
    },
    email:{
      field : 'email',
      type : DataTypes.STRING,
      allowNull: false,
    },
    username : {
      field : 'username',
      type : DataTypes.STRING,
      allowNull: false,
    },
    password : {
      field : 'password',
      type : DataTypes.STRING,
      allowNull: true,
    },
    userStatus : {
      field : 'user_status',
      type : DataTypes.ENUM,
      values: ['pending', 'active', 'suspended', 'deleted'],
      defaultValue : 'pending',
      allowNull: false,
    },
    userVerificationStatud : {
      field : 'user_verification_status',
      type : DataTypes.ENUM,
      values: ['pending', 'verified', 'un_verified'],
      defaultValue : 'pending',
      allowNull: false,
    }
  }, {
    timestamps: true,
	  freezeTableName: true,
  	tableName: 't_users',
  	verion : true
  });
  user.associate = function(models) {
    // associations can be defined here
  };
  return user;
};