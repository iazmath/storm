'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.json')[env];
const db = {};

let sequelize;

if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, config);
}

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = sequelize['import'](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

//alternative configuration
//https://www.codementor.io/mirko0/how-to-use-sequelize-with-node-and-express-i24l67cuz

//added
//https://hackernoon.com/getting-started-with-sequelize-for-nodejs-applications-2854c58ffb8c
sequelize
		.authenticate()
		.then(() => {
			console.log('Connection has been established successfully.');
		})
		.catch((err) => {
			console.log('Unable to connect to the database:', err);
		});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.test = require('./test')(sequelize, Sequelize);

db.student = require('./student')(sequelize, Sequelize);
db.studentCategory = require('./student.category')(sequelize, Sequelize);

db.student.belongsTo(db.studentCategory, {foreignKey: 'studentCategoryId' });
db.studentCategory.hasMany(db.student, {foreignKey: 'studentCategoryId'})

module.exports = db;
