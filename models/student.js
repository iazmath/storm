'use strict';
module.exports = (sequelize, DataTypes) => {
  const student = sequelize.define('student', {
    id: {
      field: 'id',
      type : DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true
    },
    firstName: {
      field: 'first_name',
      type: DataTypes.STRING
    },
    lastName: {
      field: 'last_name',
      type: DataTypes.STRING,
    },
    email: {
      field : 'email',
      type: DataTypes.STRING
    },
    studentCategoryId : {
      field : 'studnet_category_id',
      type : DataTypes.INTEGER,
      // references: {
      //   model: require('../models/student.category'),
      //   key: "id"
      // }
    },
    createdAt : {
      field : 'created_at',
      type : DataTypes.DATE
    },
    updatedAt : {
      field : 'updated_at',
      type : DataTypes.DATE
    }
  },
  {
    timestamps: true,
	  freezeTableName: true,
  	tableName: 't_students',
  	verion : true
  });
  student.associate = function(models) {

  };
  return student;
};