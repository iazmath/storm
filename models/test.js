'use strict';
module.exports = (sequelize, DataTypes) => {
  const t_tests = sequelize.define('test', {
    id: {
      field: 'id',
      type : DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    createdAt : DataTypes.DATE,
    updatedAt : DataTypes.DATE
  }, {
    timestamps: false,
	  freezeTableName: true,
  	tableName: 't_tests',
  	verion : true
  });
  t_tests.associate = function(models) {
    // associations can be defined here
  };
  return t_tests;
};