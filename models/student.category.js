'use strict';
module.exports = (sequelize, DataTypes) => {
  const studentCategory = sequelize.define('studentCategory', {
    id: {
      field : 'id',
      type : DataTypes.INTEGER,
      autoIncrement : true,
      primaryKey : true
    },
    name : {
      field : 'name',
      type  : DataTypes.STRING,
      allowNull : false
    },
  }, {
    timestamps: true,
	  freezeTableName: true,
  	tableName: 't_student_categories',
  	verion : true
  });
  studentCategory.associate = function(models) {
  };
  return studentCategory;
};